import SAX.SaxHelper;
import SAX.SaxUtil;
import SAX.Util.CutType;
import SAX.Util.MethodType;
import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.ITracePoint2D;
import info.monitorenter.gui.chart.ZoomableChart;
import info.monitorenter.gui.chart.demos.Showcase;
import info.monitorenter.gui.chart.events.Chart2DActionSaveImageSingleton;
import info.monitorenter.gui.chart.io.FileFilterExtensions;
import info.monitorenter.gui.chart.traces.Trace2DSimple;
import org.renjin.script.RenjinScriptEngineFactory;
import org.rosuda.JRI.REXP;
import org.rosuda.JRI.Rengine;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import javax.imageio.ImageIO;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import javax.swing.*;
import javax.swing.filechooser.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.*;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by IN0120 on 08-05-2017.
 */

public class HotTree extends java.applet.Applet {

    //Debug
    boolean FULL_DEBUG = false;
    boolean FILE_DEBUG = true;
    boolean SHOW_IN_GUI = false;
    boolean SHOW_EXTRA_INFO = false;

    //Files
    private static String csvInFile, csvOutFile, csvOut1File;

    boolean IS_RUNNING = false;

    //Setting
    int WINDOW_SIZE = 145;
    int PAA_SIZE = 8;
    int ALPHABET_SIZE_SAT = 5;
    int NUM_ANOMALY_HOTSAX = 20;
    int NUM_ANOMALY_SAT = 0;
    double Z_NORM_THRESHOLD = 0.01;
    String TIME_STAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";
    DecimalFormat brf = new DecimalFormat("#.00");
    int SAT_SEGMENT_METHOD = 0;
    int NUM_TOP_MOTIFS = 7;

    private static SimpleDateFormat df;
    CutType mSatCutType = CutType.LINEAR_CUT;
    boolean HAS_TIMESTAMP = true;
    boolean HAS_HEADERS = false;
    int TIME_STAMP_COL = 0;
    int VALUE_COL = 1;

    //Data
    int total_sequences;
    int REPLOT_OFFSET = 100;
    double[] satCutsArray;
    ArrayList<Double> zNormArray = new ArrayList<>();               //z-Norm Time Series Dates
    ArrayList<Double> mTimeSeriesValueList = new ArrayList<>();     //Time Series Dates
    ArrayList<Long> mTimeSeriesDateList = new ArrayList<>();        //Time Series Dates
    ArrayList<String> mSatSubsList = new ArrayList<>();
    double[][] mHotSaxPaaArray;
    ArrayList<Asset360_HOTSAX> asset360HotSaxArrayListNnSorted;
    ArrayList<Asset360_HOTSAX> asset360HotSaxArrayList;
    ArrayList<Asset360_HOTTREE> asset360HotTreeArrayList;
    char[] satString;
    String[] sat360SeqArray;
    int[] sat360ProbArray;
    ByteArrayOutputStream consoleOut;
    double[] customSatCutArray;
    int seg_size;
    double maxConfidence = 0;
    double minConfidence = Double.MAX_VALUE;
    int plottype = 2;                             //0-HOTSAX, 1-SAT, 2-HOTTREE

    //Setting Low
    private static String ANOMALIES = "ANOMALIES";
    private static String SEQUENCE_DETAIL = "SEQUENCE_DETAIL";

    //Utils
    private static String line = "";
    private static String comma = ",";
    private static String space = " ";
    private static String commaSpace = ", ";
    private static String underscore = "_";
    private static String nl = "\n";
    private static String txt = ".txt";
    private static String csv = ".csv";
    private static String rootDir = "C:\\Users\\in0120\\Desktop\\python test\\201B_GEARBOX_60S_WITH_TIME_DT_REMOVED";

    private static String dir;
    private static String fileName;
    private static String fileWoEx;

    //Info
    private static long startTime;


    //GUI
    private TextField window_size;
    private TextField paa_size;
    private TextField alphabet_size_sat;
    private TextField num_anomaly;
    private Checkbox has_time_stamps, has_headers;
    private TextField time_stamp_format;
    private TextField sat_segment_method;
    private TextField csvInputTF, csvOutputTF, customBreakpointsTF;
    private Button runHotSaxButton, runSatButton, runHotTreeButton, nextPlot, browseButton, rePlotButton, viewTsButton, getBreakpointsButton;
    private TextArea consoleText;
    private Chart2D chart = new Chart2D();
    private JSlider anomalySlider;

    private void runHotTree() throws Exception, Error {

        startTime = System.currentTimeMillis();
        IS_RUNNING = true;

        if (!FULL_DEBUG && SHOW_IN_GUI) {
            consoleOut = new ByteArrayOutputStream();
            System.setOut(new PrintStream(consoleOut));
            System.setErr(new PrintStream(consoleOut));
        }

        println("Chosen File: " + csvInFile);

        //Precaution
        browseButton.disable();
        runHotSaxButton.disable();
        runSatButton.disable();
        runHotTreeButton.disable();
        nextPlot.disable();
        anomalySlider.disable();
        rePlotButton.disable();
        viewTsButton.disable();

        clearVariables();
        getSettingsFromUI();

        //Set anomaly slider range
        int ano_min = NUM_ANOMALY_HOTSAX - (REPLOT_OFFSET);
        if (ano_min < 0) ano_min = 0;
        anomalySlider.setMaximum(NUM_ANOMALY_HOTSAX + REPLOT_OFFSET);
        anomalySlider.setMinimum(ano_min);
        anomalySlider.setValue(NUM_ANOMALY_HOTSAX);

        readTimeSeries();
        println("Input points: " + mTimeSeriesValueList.size());

        //Z-Norm && Get Cuts Array
        if (mSatCutType == CutType.GAUSSIAN_CUT)
            zNormArray = SaxUtil.znorm(mTimeSeriesValueList, Z_NORM_THRESHOLD);
        else
            zNormArray.addAll(mTimeSeriesValueList);

        //PAA Array || SAX String
        float char_length = WINDOW_SIZE / (float) PAA_SIZE;   //PAA String Length
        println("Char length: " + char_length);
        double[] paaArray = SaxHelper.generatePaaArray(zNormArray, (int) (mTimeSeriesValueList.size() / char_length));
        println("PAA Array Length: " + paaArray.length);


        double[] paaLimits = new double[2];
        paaLimits[0] = SaxUtil.min(paaArray);
        paaLimits[1] = SaxUtil.max(paaArray);

        satCutsArray = SaxUtil.generateCuts(zNormArray, paaLimits, mSatCutType, ALPHABET_SIZE_SAT, MethodType.SAT, customSatCutArray);
        println("SAT Cuts Array:\n" + Arrays.toString(satCutsArray));

        satString = SaxHelper.timeSeriesToString(paaArray, satCutsArray);
        println("SAT String Length: " + satString.length);

        println("SAT String: " + new String(satString));

        //FixMe Save PAA Array
        if (FULL_DEBUG || FILE_DEBUG) {
            String str1 = dir + fileWoEx + underscore + "SAT_STRING" + txt;
            File files1 = new File(str1);
            PrintWriter writers1 = new PrintWriter(files1, "UTF-8");
            writers1.print(new String(satString));
            writers1.close();
        }
        if (FULL_DEBUG || FILE_DEBUG) {
            StringBuilder sb = new StringBuilder();
            for (double aPaaArray : paaArray) {
                sb.append(aPaaArray).append(nl);
            }
            sb.setLength(sb.length() - 1);
            String str = dir + fileWoEx + underscore + "PAA_ARRAY" + txt;
            File files = new File(str);
            PrintWriter writers = new PrintWriter(files, "UTF-8");
            writers.print(sb.toString());
            writers.close();
        }
        if (FULL_DEBUG || SHOW_EXTRA_INFO) {
            print(new String(satString));
        }

        //Generate Subsequent Array
        StringBuilder satSubsequentSb;
        mHotSaxPaaArray = new double[paaArray.length - PAA_SIZE + 1][PAA_SIZE];
        for (int i = 0; i <= (paaArray.length - PAA_SIZE); i++) {
            satSubsequentSb = new StringBuilder("");
            for (int j = 0; j < PAA_SIZE; j++) {
                satSubsequentSb.append(satString[i + j]);
                mHotSaxPaaArray[i][j] = paaArray[i + j];
            }
            mSatSubsList.add(satSubsequentSb.toString());
        }

        total_sequences = mSatSubsList.size();
        println("No. of SAT String Sequences : " + mSatSubsList.size());
        println("No. of HOTSAX PAA Array Sequences : " + mHotSaxPaaArray.length);

        if (FULL_DEBUG || FILE_DEBUG) {
            StringBuilder satStrSb = new StringBuilder("");
            for (String aMSatSubsList : mSatSubsList) {
                satStrSb.append(aMSatSubsList).append(nl);
            }
            writeStringTofile(satStrSb.toString(), "SAT_SEQ");
        }
        if (FULL_DEBUG || FILE_DEBUG) {
            StringBuilder satStrSb = new StringBuilder("");
            for (double[] aMSatSubsList : mHotSaxPaaArray) {
                satStrSb.append(Arrays.toString(aMSatSubsList)).append(nl);
            }
            writeStringTofile(satStrSb.toString(), "SAT_SEQ1");
        }

        /*print("\nmHotSaxSubsList: ");
        for (String s : mHotSaxSubsList) {
            print(s);
        }
        print("\nmSatSubsList: ");
        for (String s : mSatSubsList) {
            print(s);
        }
        print("mSatSubsList Size: " + mSatSubsList.size());*/

        //Generate NN Array & NN Mean
        double[] nnArray = new double[paaArray.length - (2 * (PAA_SIZE - 1))];
        for (int i = 0; i < nnArray.length; i++) {
            //nnArray[i] = SaxHelper.getNnDistStrings(mHotSaxSubsList.get(i + (PAA_SIZE - 1)), mHotSaxSubsList.get(i));
            nnArray[i] = SaxHelper.getNnDistPaa(mHotSaxPaaArray[i], mHotSaxPaaArray[i + (PAA_SIZE - 1)]);
        }

        println("NN Array Length: " + nnArray.length);

        if (FULL_DEBUG || FILE_DEBUG) {
            StringBuilder sb = new StringBuilder();
            for (double aNnArray : nnArray) {
                sb.append(aNnArray).append(nl);
            }
            sb.setLength(sb.length() - 1);
            String str = dir + fileWoEx + underscore + "NN_ARRAY" + txt;
            File files = new File(str);
            PrintWriter writers = new PrintWriter(files, "UTF-8");
            writers.print(sb.toString());
            writers.close();
        }


        //Create Time Stamps
        seg_size = mTimeSeriesValueList.size() / paaArray.length;
        int start_index = 0;
        String[] timeStampArray = new String[paaArray.length];
        int[] posArray = new int[paaArray.length];
        for (int i = 0; i < paaArray.length; i++) {
            if (HAS_TIMESTAMP) timeStampArray[i] = df.format(new Date(mTimeSeriesDateList.get(start_index)));
            posArray[i] = start_index;
            start_index = start_index + seg_size;
        }

        //Create Anomaly List (Pos sorted & NN sorted)
        asset360HotSaxArrayList = new ArrayList<>();
        asset360HotSaxArrayListNnSorted = new ArrayList<>();

        for (int i = 0; i < nnArray.length; i++) {
            asset360HotSaxArrayList.add(new Asset360_HOTSAX(i, posArray[i], WINDOW_SIZE, timeStampArray[i], nnArray[i]));
            //print(posArray[i]+" "+ WINDOW_SIZE_HOTSAX+" "+ mSaxSubsList.get(i)+" "+ timeStampArray[i]+" "+ nnArray[i]);
        }
        asset360HotSaxArrayListNnSorted.addAll(asset360HotSaxArrayList);
        asset360HotSaxArrayListNnSorted.sort((a, b) -> Double.compare(b.getNn(), a.getNn()));

        //print("asset360HotSaxArrayListNnSorted: " + asset360HotSaxArrayListNnSorted.size());

        //print("Running SAT\n");
        runSAT();

        getCombinedConfidence();

        //Print Asset 360
        /*int breakAnomalyCount = NUM_ANOMALY_HOTSAX;
        if (asset360HotSaxArrayListNnSorted.size() < breakAnomalyCount)
            breakAnomalyCount = asset360HotSaxArrayListNnSorted.size();*/

        int breakAnomalyCount = asset360HotSaxArrayListNnSorted.size();
        StringBuilder sequenceDetailSb = new StringBuilder("");
        sequenceDetailSb.append("Sequence Index").append(comma).append("Position").append(comma).append("Window Start Date").append(comma).append("HOTTREE CONFIDENCE").append(comma).append("HOTSAX NN Distance").append(comma).append("Sequence Occurrence").append(comma).append("SAT Probability %").append(comma).append("SAT Sequence").append(comma).append("Window Length");
        for (int i = 0; i < breakAnomalyCount; i++) {
            Asset360_HOTSAX asset360Hotsax = asset360HotSaxArrayListNnSorted.get(i);
            sequenceDetailSb.append(nl).append(asset360Hotsax.getIndex()).append(comma).append(asset360Hotsax.getPosition()).append(comma).append(asset360Hotsax.getDate()).append(comma).append(asset360Hotsax.getNn() / (double) sat360ProbArray[asset360Hotsax.getIndex()]).append(comma).append(asset360Hotsax.getNn()).append(comma).append(sat360ProbArray[asset360Hotsax.getIndex()]).append(comma).append(getProbPercentage(sat360ProbArray[asset360Hotsax.getIndex()])).append(comma).append(sat360SeqArray[asset360Hotsax.getIndex()]).append(comma).append(WINDOW_SIZE);
        }

        csvOut1File = dir + fileWoEx + underscore + SEQUENCE_DETAIL + underscore + "W-" + WINDOW_SIZE + underscore + "P-" + PAA_SIZE + underscore + "a-SAT-" + ALPHABET_SIZE_SAT + csv;
        println("Sequence Detail File:\n" + csvOut1File);

        File file = new File(csvOut1File);
        PrintWriter writer = new PrintWriter(file, "UTF-8");
        writer.print(sequenceDetailSb);
        writer.close();

        StringBuilder asset360Sb = new StringBuilder("");
        asset360Sb.append("Sequence Index").append(comma).append("Position").append(comma).append("Window Start Date").append(comma).append("HOTTREE CONFIDENCE").append(comma).append("HOTSAX NN Distance").append(comma).append("Sequence Occurrence").append(comma).append("SAT Probability %").append(comma).append("SAT Sequence").append(comma).append("Window Length");
        for (int i = 0; i < asset360HotTreeArrayList.size(); i++) {
            Asset360_HOTTREE asset360_hottree = asset360HotTreeArrayList.get(i);
            asset360Sb.append(nl).append(asset360_hottree.getIndex()).append(comma).append(asset360_hottree.getPosition()).append(comma).append(asset360_hottree.getDate()).append(comma).append(asset360_hottree.getConfidence()).append(comma).append(asset360_hottree.getNn()).append(comma).append(sat360ProbArray[asset360_hottree.getIndex()]).append(comma).append(getProbPercentage(sat360ProbArray[asset360_hottree.getIndex()])).append(comma).append(sat360SeqArray[asset360_hottree.getIndex()]).append(comma).append(WINDOW_SIZE);
        }

        csvOutFile = dir + fileWoEx + underscore + ANOMALIES + underscore + "W-" + WINDOW_SIZE + underscore + "P-" + PAA_SIZE + underscore + "a-SAT-" + ALPHABET_SIZE_SAT + csv;
        println("Anomaly File:\n" + csvOutFile);

        File file1 = new File(csvOutFile);
        PrintWriter writer1 = new PrintWriter(file1, "UTF-8");
        writer1.print(asset360Sb);
        writer1.close();

        //Print HOTSAX Anomalies
        plotHotSaxAnomalies(false, false);

        long endTime = System.currentTimeMillis();
        println("Execution Time: " + (endTime - startTime) / 1000.0 + " seconds");

        runHotSaxButton.enable();
        runSatButton.enable();
        runHotTreeButton.enable();
        nextPlot.enable();
        browseButton.enable();
        anomalySlider.enable();
        rePlotButton.enable();
        viewTsButton.enable();

        if (!FULL_DEBUG && SHOW_IN_GUI) consoleText.setText(consoleOut.toString());
        //Desktop.getDesktop().open(new File(csvOutFile));

        IS_RUNNING = false;

        Object[] options = {"Show Asset360 file", "Show Sequence Detail file", "CANCEL"};
        int optVal = JOptionPane.showOptionDialog(null, "Execution Time: " + (endTime - startTime) / 1000.0 + " seconds", "Done !!!",
                JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE,
                null, options, options[0]);
        if (optVal == 0) showFile(csvOutFile);
        else if (optVal == 1) showFile(csvOut1File);

    }

    private void showFile(String file) throws IOException {
        Runtime.getRuntime().exec(new String[]{
                "explorer.exe",
                "/select,",
                "\"" + file + "\""});
    }

    private void getCombinedConfidence() {
        asset360HotTreeArrayList = new ArrayList<>();
        for (int i = 0; i < sat360ProbArray.length; i++) {
            double satOccur = getProbPercentage(sat360ProbArray[i]);
            if (satOccur <= 1 && i < asset360HotSaxArrayListNnSorted.size()) {
                Asset360_HOTSAX asset360_hotsax = asset360HotSaxArrayList.get(i);
                double con = asset360_hotsax.getNn() / (double) sat360ProbArray[i];
                if (con > maxConfidence) maxConfidence = con;
                //if (con < minConfidence && con != 0) minConfidence = con;
                if (con < minConfidence) minConfidence = con;
                asset360HotTreeArrayList.add(new Asset360_HOTTREE(i, asset360_hotsax.getPosition(), asset360_hotsax.getDate(), con, asset360_hotsax.getNn(), sat360ProbArray[i], getProbPercentage(sat360ProbArray[i]), sat360SeqArray[i], asset360_hotsax.getLength()));
            }
        }
    }

    private void writeStringTofile(String satStrSb, String sat_str) throws FileNotFoundException, UnsupportedEncodingException {
        String out = dir + fileWoEx + underscore + sat_str + underscore + "W-" + WINDOW_SIZE + underscore + "P-" + PAA_SIZE + underscore + "a-SAT-" + ALPHABET_SIZE_SAT + txt;
        File files = new File(out);
        PrintWriter writers = new PrintWriter(files, "UTF-8");
        writers.print(satStrSb);
        writers.close();
    }

    private double getProbPercentage(int pr) {
        return (pr * 100) / (double) total_sequences;
    }

    private void clearVariables() {
        REPLOT_OFFSET = 100;
        satCutsArray = null;
        zNormArray = new ArrayList<>();
        mTimeSeriesValueList = new ArrayList<>();
        mTimeSeriesDateList = new ArrayList<>();
        mSatSubsList = new ArrayList<>();

        asset360HotSaxArrayListNnSorted = new ArrayList<>();
        asset360HotSaxArrayList = new ArrayList<>();
        asset360HotTreeArrayList = new ArrayList<>();
        maxConfidence = 0;
        minConfidence = Double.MAX_VALUE;
    }

    private void getSettingsFromUI() {
        customSatCutArray = null;
        WINDOW_SIZE = Integer.parseInt(window_size.getText().trim());
        PAA_SIZE = Integer.parseInt(paa_size.getText().trim());
        ALPHABET_SIZE_SAT = Integer.parseInt(alphabet_size_sat.getText().trim());
        NUM_ANOMALY_HOTSAX = Integer.parseInt(num_anomaly.getText().trim());
        HAS_TIMESTAMP = has_time_stamps.getState();
        HAS_HEADERS = has_headers.getState();
        TIME_STAMP_FORMAT = time_stamp_format.getText().trim();
        SAT_SEGMENT_METHOD = Integer.parseInt(sat_segment_method.getText().trim());

        if (SAT_SEGMENT_METHOD == 0) {
            mSatCutType = CutType.LINEAR_CUT;
            String tmp = customBreakpointsTF.getText().trim();
            if (tmp != null && !tmp.equals("")) {
                String[] temp = tmp.split(",");
                customSatCutArray = new double[temp.length];
                for (int i = 0; i < customSatCutArray.length; i++) {
                    customSatCutArray[i] = Double.parseDouble(temp[i]);
                }
            }
        } else if (SAT_SEGMENT_METHOD == 1) {
            mSatCutType = CutType.GAUSSIAN_CUT;
        } else if (SAT_SEGMENT_METHOD == 2) {
            mSatCutType = CutType.CUSTOM_CUT;
            String[] temp = customBreakpointsTF.getText().split(",");
            customSatCutArray = new double[temp.length];
            for (int i = 0; i < customSatCutArray.length; i++) {
                customSatCutArray[i] = Double.parseDouble(temp[i]);
            }
        }

        if (HAS_TIMESTAMP) {
            TIME_STAMP_COL = 0;
            VALUE_COL = 1;
        } else VALUE_COL = 0;

        df = new SimpleDateFormat(TIME_STAMP_FORMAT);

        println("WINDOW_SIZE: " + WINDOW_SIZE + commaSpace + "PAA_SIZE: " + PAA_SIZE + commaSpace + commaSpace + "a-SAT-Size: " + ALPHABET_SIZE_SAT + commaSpace + "HAS_TIMESTAMP?: " + commaSpace + HAS_TIMESTAMP + commaSpace + "NUM_ANOMALY: " + NUM_ANOMALY_HOTSAX + commaSpace + "TIME_STAMP_FORMAT: " + TIME_STAMP_FORMAT + commaSpace + "SATCutType: " + mSatCutType);
    }

    private void readTimeSeries() throws ParseException, Error {
        try (BufferedReader br = new BufferedReader(new FileReader(csvInFile))) {
            while ((line = br.readLine()) != null) {
                if (!HAS_HEADERS) {
                    if (HAS_TIMESTAMP) {
                        String[] arr = line.split(comma);
                        mTimeSeriesDateList.add(df.parse(arr[TIME_STAMP_COL]).getTime());
                        mTimeSeriesValueList.add(Double.parseDouble(arr[VALUE_COL]));
                    } else {
                        mTimeSeriesValueList.add(Double.parseDouble(line));
                    }
                } else
                    HAS_HEADERS = false;
            }

        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Error: " + e.getMessage());
        }
    }

    //FixMe##################### SAT #######################//

    private void runSAT() throws Exception, Error {
        ArrayList<SubProbObject> probList = new ArrayList<>();
        ArrayList<Integer> subIndexList = new ArrayList<>();
        int subIndex;
        for (String aMSatSubsList : mSatSubsList) {
            subIndex = findSubIndex(aMSatSubsList, 1);

            if (!subIndexList.contains(subIndex)) {
                subIndexList.add(subIndex);
                probList.add(new SubProbObject(subIndex, aMSatSubsList, 1));
            } else {
                SubProbObject subProbObject = probList.get(subIndexList.indexOf(subIndex));
                subProbObject.setProb(subProbObject.getProb() + 1);
            }
        }

        //Sort probability list high to low
        probList.sort((a, b) -> Integer.compare(b.getProb(), a.getProb()));

        print("TOP MOTIFS:");
        for (int i = 0; i < NUM_TOP_MOTIFS; i++) {
            print(probList.get(i).getSUB());
        }

        sat360SeqArray = new String[mSatSubsList.size()];
        sat360ProbArray = new int[mSatSubsList.size()];

        String seq;
        int prob;
        for (SubProbObject subProbObject : probList) {
            seq = subProbObject.getSUB();
            prob = subProbObject.getProb();
            for (int i = 0; i < mSatSubsList.size(); i++) {
                if (mSatSubsList.get(i).equals(seq)) {
                    sat360SeqArray[i] = seq;
                    sat360ProbArray[i] = prob;
                }
            }
        }

        writeStringTofile(Arrays.toString(sat360SeqArray), "sat360SeqArray");
        writeStringTofile(Arrays.toString(sat360ProbArray), "sat360ProbArray");
    }

    //FixME SAT Helpers
    private int findSubIndex(String sax_sub, int stage) {
        if (stage == PAA_SIZE) return getVal(sax_sub.charAt(stage - 1));
        else {
            int prepoints = ((int) Math.pow(ALPHABET_SIZE_SAT, PAA_SIZE - stage)) * (getVal(sax_sub.charAt(stage - 1)) - 1);
            //System.out.println("prepoints: " + prepoints);
            return prepoints + findSubIndex(sax_sub, stage + 1);
        }
    }

    private int getVal(char c) {
        return Character.getNumericValue(c) - 9;
    }

    private void plotHotSaxAnomalies(boolean re, boolean rehot) {

        int plot_break_counter = 0;
        if (rehot) plottype = 0;
        if (re) {
            runHotSaxButton.disable();
            runSatButton.disable();
            runHotTreeButton.disable();
            nextPlot.disable();
            browseButton.disable();
            anomalySlider.disable();
            rePlotButton.disable();
            viewTsButton.disable();
        }

        chart.removeAllTraces();
        ITrace2D trace = new Trace2DSimple();

        chart.addTrace(trace);
        trace.setZIndex(1);
        if (plottype == 2) trace.setColor(Color.ORANGE);
        else trace.setColor(Color.BLACK);
        String plotname = "";
        if (plottype == 0) plotname = fileWoEx + "_1_HOTSAX";
        else if (plottype == 1) plotname = fileWoEx + "_2_SAT";
        else if (plottype == 2) plotname = fileWoEx + "_3_HOTTREE";

        trace.setName(plotname);

        //FixME -> plot timeSeries
        for (int i = 0; i < mTimeSeriesValueList.size(); i++) {
            trace.addPoint(i, mTimeSeriesValueList.get(i));
        }

        if (plottype == 0) {
            plot_break_counter = 0;
            for (Asset360_HOTSAX asset360_hotsax : asset360HotSaxArrayListNnSorted) {
                trace = new Trace2DSimple();
                chart.addTrace(trace);
                trace.setZIndex(4);
                trace.setColor(Color.RED);
                trace.setName("");
                for (int i = asset360_hotsax.getPosition(); i < asset360_hotsax.getPosition() + WINDOW_SIZE; i++) {
                    trace.addPoint(i, mTimeSeriesValueList.get(i));
                }
                plot_break_counter++;
                if (plot_break_counter >= NUM_ANOMALY_HOTSAX) break;
            }

            plot_break_counter = 0;
            for (Asset360_HOTSAX asset360_hotsax : asset360HotSaxArrayListNnSorted) {
                //if ((asset360_hotsax.getPosition() + WINDOW_SIZE_HOTSAX + WINDOW_SIZE_HOTSAX) >= mTimeSeriesValueList.size()) continue;
                trace = new Trace2DSimple();
                chart.addTrace(trace);
                trace.setZIndex(3);
                trace.setColor(Color.ORANGE);
                trace.setName("");
                for (int i = asset360_hotsax.getPosition() + WINDOW_SIZE; i < asset360_hotsax.getPosition() + WINDOW_SIZE + WINDOW_SIZE; i++) {
                    if (i >= mTimeSeriesValueList.size()) continue;
                    trace.addPoint(i, mTimeSeriesValueList.get(i));
                }
                plot_break_counter++;
                if (plot_break_counter >= NUM_ANOMALY_HOTSAX) break;
            }
        }

        if (plottype == 1) {
            ArrayList<Asset360_HOTTREE> asset360_hottrees = new ArrayList<>();
            asset360_hottrees.addAll(asset360HotTreeArrayList);
            //asset360_hottrees.sort((a, b) -> Double.compare(a.getProb_per(), b.getConfidence()));
            for (Asset360_HOTTREE asset360_hottree : asset360_hottrees) {
                trace = new Trace2DSimple();
                chart.addTrace(trace);
                trace.setZIndex(2);
                trace.setStroke(new BasicStroke(1));
                trace.setColor(Color.GREEN);
                trace.setName("");
                for (int j = asset360_hottree.getPosition(); j < asset360_hottree.getPosition() + WINDOW_SIZE; j++) {
                    if (j >= mTimeSeriesValueList.size()) continue;
                    trace.addPoint(j, mTimeSeriesValueList.get(j));
                }

            }
        }

        if (plottype == 2) {
            plot_break_counter = 0;
            asset360HotTreeArrayList.sort((a, b) -> Double.compare(b.getConfidence(), a.getConfidence()));
            for (Asset360_HOTTREE asset360_hottree : asset360HotTreeArrayList) {
                if (asset360_hottree.getConfidence() == 0) continue;
                double con = asset360_hottree.getConfidence();
                //int alpha = 255;
                int alpha = (int) ((255 * (con - minConfidence)) / (maxConfidence - minConfidence));

                trace = new Trace2DSimple();
                chart.addTrace(trace);
                trace.setZIndex(2);
                trace.setStroke(new BasicStroke(2));
                trace.setColor(new Color(0, 0, 0, alpha));
                trace.setName("");
                for (int j = asset360_hottree.getPosition(); j < asset360_hottree.getPosition() + asset360_hottree.getLength(); j++) {
                    if (j >= mTimeSeriesValueList.size()) continue;
                    trace.addPoint(j, mTimeSeriesValueList.get(j));
                }
                plot_break_counter++;
                if (plot_break_counter >= NUM_ANOMALY_HOTSAX) break;
            }
        }

        if (re) {
            runHotSaxButton.enable();
            runSatButton.enable();
            runHotTreeButton.enable();
            nextPlot.enable();
            browseButton.enable();
            anomalySlider.enable();
            rePlotButton.enable();
            viewTsButton.enable();
        }

        saveImage(plotname);
    }

    private void saveImage(String p) {
        BufferedImage img = chart.snapShot();

        ImageWriter imgWriter = ImageIO.getImageWritersBySuffix("png").next();
        ImageWriteParam params = imgWriter.getDefaultWriteParam();
        if (params.canWriteCompressed()) {
            params.setCompressionMode(0);
        }

        try {
            imgWriter.setOutput(new FileImageOutputStream(new File(dir + p + ".png")));
            imgWriter.write(img);
        } catch (IOException var15) {
            var15.printStackTrace();
        }
    }

    private void browseInputFile() {
        JFileChooser chooser = new JFileChooser(rootDir);
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "Comma separated csv files", "csv");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            csvInFile = chooser.getSelectedFile().getAbsolutePath();
            dir = csvInFile.substring(0, csvInFile.lastIndexOf("\\") + 1);
            fileName = chooser.getSelectedFile().getName();
            fileWoEx = fileName.substring(0, fileName.lastIndexOf("."));
            csvOutFile = dir + fileWoEx + underscore + ANOMALIES + underscore + "W-" + WINDOW_SIZE + underscore + "P-" + PAA_SIZE + underscore + "a-SAT-" + ALPHABET_SIZE_SAT + csv;

            csvInputTF.setText(csvInFile);
            csvOutputTF.setText(csvOutFile);
            runHotSaxButton.enable();
            runSatButton.enable();
            runHotTreeButton.enable();
        }
    }

    /******************************************************************/

    private static void print(String s) {
        System.out.println(s);
    }

    private static void println(String s) {
        System.out.println("");
        System.out.println(s);
    }

    public static void main(String[] args) {
        HotTree hotTree = new HotTree();
        JFrame myFrame = new JFrame("HotTree");
        myFrame.add(hotTree);
        myFrame.pack();
        myFrame.setSize(1300, 720);
        myFrame.setVisible(true);
        myFrame.addWindowListener(
                new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        System.exit(0);
                    }
                }
        );
        hotTree.init();

    }

    public void init() {
        setLayout(null);

        Label window_size_label = new Label("Window Size: ");
        Label paa_size_label = new Label("PAA Size: ");
        Label alphabet_size_sat_label = new Label("α (Alphabet Size SAT): ");
        window_size = new TextField(String.valueOf(WINDOW_SIZE));
        paa_size = new TextField(String.valueOf(PAA_SIZE));
        alphabet_size_sat = new TextField(String.valueOf(ALPHABET_SIZE_SAT));
        Label num_anomaly_label = new Label("No. Anomaly: ");
        num_anomaly = new TextField(String.valueOf(NUM_ANOMALY_HOTSAX));
        has_time_stamps = new Checkbox("Has time stamps?", HAS_TIMESTAMP);
        has_headers = new Checkbox("Has headers?", HAS_HEADERS);
        Label time_stamp_format_label = new Label("Time Format: ");
        time_stamp_format = new TextField(String.valueOf(TIME_STAMP_FORMAT));
        getBreakpointsButton = new Button("Get Breaks");
        Label input_label = new Label("Input File");

        Label sat_segment_method_label = new Label("SAT Segment Method:");
        sat_segment_method = new TextField(String.valueOf(SAT_SEGMENT_METHOD));
        Label segment_help_label = new Label("(0-Linear, 1-Gaussian,");
        Label segment_help_label1 = new Label("2-Custom)");
        Label custom_segment_breakpoints_label = new Label("Custom Segment Breakpoints (Separate by comma):");
        customBreakpointsTF = new TextField("");

        csvInputTF = new TextField("");
        csvInputTF.setEnabled(false);
        browseButton = new Button("Browse");
        Label output_label = new Label("Output File");
        csvOutputTF = new TextField("");
        csvOutputTF.setEnabled(false);
        runHotSaxButton = new Button("RUN HOTSAX");
        runSatButton = new Button("RUN SAT");
        runHotTreeButton = new Button("RUN HOTTREE");
        nextPlot = new Button("NEXT PLOT");
        consoleText = new TextArea();
        Label change_anomalies_label = new Label("Change no. anomalies: ");
        int ano_min = NUM_ANOMALY_HOTSAX - (REPLOT_OFFSET);
        if (ano_min < 0) ano_min = 0;
        anomalySlider = new JSlider(JSlider.HORIZONTAL, ano_min, NUM_ANOMALY_HOTSAX + REPLOT_OFFSET, NUM_ANOMALY_HOTSAX);
        anomalySlider.setMajorTickSpacing(5);
        anomalySlider.setMinorTickSpacing(1);
        anomalySlider.setPaintTicks(true);
        anomalySlider.setPaintLabels(true);
        anomalySlider.addChangeListener(e -> {
            if (!IS_RUNNING) {
                num_anomaly.setText("" + anomalySlider.getValue());
                NUM_ANOMALY_HOTSAX = anomalySlider.getValue();
            }
        });
        anomalySlider.disable();
        rePlotButton = new Button("ReDraw Hotsax Plot");
        rePlotButton.disable();
        viewTsButton = new Button("View TS");

        window_size_label.setBounds(20, 20, 80, 25);
        window_size.setBounds(100, 20, 45, 25);
        paa_size_label.setBounds(165, 20, 60, 25);
        paa_size.setBounds(225, 20, 25, 25);
        alphabet_size_sat_label.setBounds(270, 20, 125, 25);
        alphabet_size_sat.setBounds(405, 20, 25, 25);
        num_anomaly_label.setBounds(450, 20, 80, 25);
        num_anomaly.setBounds(530, 20, 45, 25);
        has_time_stamps.setBounds(135, 90, 120, 25);
        has_headers.setBounds(20, 90, 120, 25);
        time_stamp_format_label.setBounds(265, 90, 80, 25);
        time_stamp_format.setBounds(350, 90, 130, 25);
        getBreakpointsButton.setBounds(500, 90, 70, 25);

        sat_segment_method_label.setBounds(20, 155, 130, 25);
        sat_segment_method.setBounds(160, 160, 25, 25);
        segment_help_label.setBounds(20, 175, 130, 25);
        segment_help_label1.setBounds(20, 195, 130, 25);
        custom_segment_breakpoints_label.setBounds(245, 140, 300, 20);
        customBreakpointsTF.setBounds(245, 160, 325, 50);

        input_label.setBounds(660, 20, 65, 25);
        csvInputTF.setBounds(725, 20, 475, 25);
        browseButton.setBounds(1210, 18, 50, 25);
        output_label.setBounds(660, 90, 65, 25);
        csvOutputTF.setBounds(725, 90, 535, 25);

        runHotSaxButton.setBounds(20, 235, 122, 30);
        runSatButton.setBounds(162, 235, 122, 30);
        runHotTreeButton.setBounds(304, 235, 122, 30);
        nextPlot.setBounds(446, 235, 122, 30);
        Label console_label = new Label("Console:");
        console_label.setBounds(620, 140, 100, 25);
        consoleText.setBounds(620, 165, 640, 100);
        chart.setBounds(20, 300, 1240, 300);
        change_anomalies_label.setBounds(20, 615, 130, 30);
        anomalySlider.setBounds(160, 615, 800, 50);
        rePlotButton.setBounds(1000, 615, 140, 40);
        viewTsButton.setBounds(1150, 615, 80, 40);

        add(window_size_label);
        add(window_size);
        add(paa_size_label);
        add(paa_size);
        add(alphabet_size_sat_label);
        add(alphabet_size_sat);
        add(num_anomaly_label);
        add(num_anomaly);
        add(has_time_stamps);
        add(has_headers);
        add(time_stamp_format_label);
        add(time_stamp_format);
        add(getBreakpointsButton);

        add(sat_segment_method_label);
        add(sat_segment_method);
        add(segment_help_label);
        add(segment_help_label1);
        add(customBreakpointsTF);
        add(custom_segment_breakpoints_label);

        add(input_label);
        add(csvInputTF);
        add(browseButton);
        add(output_label);
        add(csvOutputTF);
        add(runHotSaxButton);
        add(runSatButton);
        add(runHotTreeButton);
        add(nextPlot);
        add(console_label);
        add(consoleText);
        add(chart);
        add(change_anomalies_label);
        add(anomalySlider);
        add(rePlotButton);
        add(viewTsButton);

        setSize(1300, 720);
    }

    public boolean action(Event evt, Object arg) {
        if (evt.target == runHotSaxButton || evt.target == runSatButton || evt.target == runHotTreeButton) {
            try {
                if (evt.target == runHotSaxButton) plottype = 0;
                else if (evt.target == runSatButton) plottype = 1;
                else if (evt.target == runHotTreeButton) plottype = 2;
                runHotTree();
            } catch (Exception | Error e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, "Error: " + e.getMessage());
            }

        } else if (evt.target == browseButton) {
            browseButton.disable();
            browseInputFile();
            browseButton.enable();
        } else if (evt.target == rePlotButton) {
            plotHotSaxAnomalies(true, true);
        } else if (evt.target == viewTsButton) {
            try {
                clearVariables();
                getSettingsFromUI();
                readTimeSeries();
                plotTSnewFrame();
            } catch (ParseException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, "Error: " + e.getMessage());
            }

        } else if (evt.target == getBreakpointsButton) {
            try {
                clearVariables();
                getSettingsFromUI();
                readTimeSeries();
                getBreakpointsViaR();
            } catch (ParseException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, "Error: " + e.getMessage());
            }
        } else if (evt.target == nextPlot) {
            plottype++;
            if (plottype == 3) plottype = 0;

            plotHotSaxAnomalies(true, false);
        }
        return true;
    }

    private void getBreakpointsViaR() {
        String rScriptPath = "C:/Users/in0120/Documents/R Scripts/Find Breakpoints Java Wrapper.R";

        Rengine re = new Rengine(new String[]{"--vanilla"}, false, null);
        System.out.println("Rengine created, waiting for R");

        // the engine creates R is a new thread, so we should wait until it's
        // ready
        if (!re.waitForR()) {
            System.out.println("Cannot load R");
            return;
        }
        //re.eval(text);
        re.eval("source(\"" + rScriptPath + "\")");
        String dirFor = dir.replace("\\", "/");
        re.eval("breakpoints=findBreakpoints(" + "\"" + dirFor + "\"" + "," + "\"" + fileWoEx + "\"" + ")");
        double[] breakpoints = re.eval("breakpoints").asDoubleArray();
        System.out.println(Arrays.toString(breakpoints));

        re.eval("dx=returnDx()");
        double[] dx = re.eval("dx").asDoubleArray();
        re.eval("dy=returnDy()");
        double[] dy = re.eval("dy").asDoubleArray();

        ArrayList<Double> dxList = new ArrayList<>();
        ArrayList<Double> brList = new ArrayList<>();

        println(Arrays.toString(breakpoints));
        for (double aDx : dx) {
            dxList.add(aDx);
        }
        for (double breakpoint : breakpoints) {
            brList.add(breakpoint);
        }

        ZoomableChart chart = new ZoomableChart();
        ITrace2D trace = new Trace2DSimple();
        chart.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

                if (e.getButton() == 1)
                    chart.zoomAll();
                else if (e.getButton() == 3) {
                    ITracePoint2D iTracePoint2D = chart.getPointFinder().getNearestPoint(e, chart);
                    System.out.println("X: " + iTracePoint2D.getX() + "Y: " + iTracePoint2D.getY());
                    brList.add(iTracePoint2D.getX());
                    ITrace2D trace_bp = new Trace2DSimple();
                    chart.addTrace(trace_bp);
                    trace_bp.setZIndex(2);
                    trace_bp.setStroke(new BasicStroke(5));
                    trace_bp.setColor(Color.red);
                    trace_bp.setName("");
                    trace_bp.addPoint(iTracePoint2D.getX(), iTracePoint2D.getY());
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        chart.addTrace(trace);

        for (int i = 0; i < dx.length; i++) {
            trace.addPoint(dx[i], dy[i]);
        }
        for (int i = 0; i < breakpoints.length; i++) {
            double yval = dy[dxList.indexOf(breakpoints[i])];
            ITrace2D trace_bp = new Trace2DSimple();
            chart.addTrace(trace_bp);
            trace_bp.setZIndex(2);
            trace_bp.setStroke(new BasicStroke(5));
            trace_bp.setColor(Color.red);
            trace_bp.setName("");
            trace_bp.addPoint(breakpoints[i], yval);
        }

        JFrame frame = new JFrame("State Density Plot");
        frame.getContentPane().add(chart);
        frame.setSize(1280, 300);
        frame.addWindowListener(
                new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        Collections.sort(brList);
                        StringBuilder sb = new StringBuilder("");
                        for (int i = 0; i < brList.size(); i++) {
                            if (i != 0) sb.append(comma);
                            String tempBP = brf.format(brList.get(i));
                            println(tempBP);
                            sb.append(tempBP);
                        }
                        updateUIwithBreakpoints(sb);
                    }
                }
        );
        frame.setVisible(true);
    }

    private void updateUIwithBreakpoints(StringBuilder sb) {
        sat_segment_method.setText("2");
        customBreakpointsTF.setText(sb.toString());
    }

    private void plotTSnewFrame() {
        ZoomableChart chart = new ZoomableChart();
        chart.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                chart.zoomAll();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        ITrace2D trace = new Trace2DSimple();
        chart.addTrace(trace);

        for (int i = 0; i < mTimeSeriesValueList.size(); i++) {
            trace.addPoint(i, mTimeSeriesValueList.get(i));
        }

        JFrame frame = new JFrame("Time Series");
        frame.getContentPane().add(chart);
        frame.setSize(1280, 300);
        frame.setVisible(true);
    }
}