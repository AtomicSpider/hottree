/**
 * Created by IN0120: Sanat Dutta on 08-05-2017.
 */
public class SubProbObject {

    int index;
    String SUB;
    int prob;

    public SubProbObject(int index, String SUB, int prob) {
        this.index = index;
        this.SUB = SUB;
        this.prob = prob;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getSUB() {
        return SUB;
    }

    public void setSUB(String SUB) {
        this.SUB = SUB;
    }

    public int getProb() {
        return prob;
    }

    public void setProb(int prob) {
        this.prob = prob;
    }
}
