/**
 * Created by IN0120: Sanat Dutta on 09-05-2017.
 */
public class Asset360_HOTSAX {
    int index;
    int position;
    int length;
    String date;
    double nn;

    public Asset360_HOTSAX(int index, int position, int length, String date, double nn) {
        this.index = index;
        this.position = position;
        this.length = length;
        this.date = date;
        this.nn = nn;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getNn() {
        return nn;
    }

    public void setNn(double nn) {
        this.nn = nn;
    }
}
