/**
 * Created by IN0120: Sanat Dutta on 09-05-2017.
 */
public class Asset360_HOTTREE {
    int index;
    int position;
    String date;
    double confidence;
    double nn;
    int prob;
    double prob_per;
    String seq;
    int length;


    public Asset360_HOTTREE(int index, int position, String date, double confidence, double nn, int prob, double prob_per, String seq, int length) {
        this.index = index;
        this.position = position;
        this.date = date;
        this.confidence = confidence;
        this.nn = nn;
        this.prob = prob;
        this.prob_per = prob_per;
        this.seq = seq;
        this.length = length;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getConfidence() {
        return confidence;
    }

    public void setConfidence(double confidence) {
        this.confidence = confidence;
    }

    public double getNn() {
        return nn;
    }

    public void setNn(double nn) {
        this.nn = nn;
    }

    public int getProb() {
        return prob;
    }

    public void setProb(int prob) {
        this.prob = prob;
    }

    public double getProb_per() {
        return prob_per;
    }

    public void setProb_per(double prob_per) {
        this.prob_per = prob_per;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
