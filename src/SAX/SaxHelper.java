package SAX;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by IN0120: Sanat Dutta on 22-05-2017.
 */
public class SaxHelper {

    public static double getNnDistStrings(String s, String s1) {
        double sum = 0;
        for (int i = 0; i < s.length(); i++) {
            sum += distChars(s.charAt(i), s1.charAt(i));
        }
        return sum;
    }

    public static double getNnDistPaa(double[] a, double[] b) {
        double sum = 0;
        for (int i = 0; i < a.length; i++) {
            sum += Math.pow((b[i] - a[i]), 2);
        }
        return Math.sqrt(sum);
    }

    private static int distChars(char c, char c1) {
        int pos1 = ALPHABET_STRING.indexOf(c);
        int pos2 = ALPHABET_STRING.indexOf(c1);
        return Math.abs(pos1 - pos2);
    }

    public static char[] timeSeriesToString(double[] vals, double[] cuts) {
        char[] res = new char[vals.length];
        for (int i = 0; i < vals.length; i++) {
            res[i] = num2char(vals[i], cuts);
        }
        return res;
    }

    public static char num2char(double value, double[] cuts) {
        int count = 0;
        while ((count < cuts.length) && (cuts[count] <= value)) {
            count++;
        }
        return ALPHABET[count];
    }

    public static final char[] ALPHABET = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
            'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

    public static final String ALPHABET_STRING = new String(ALPHABET);

    public static double[] generatePaaArray(ArrayList<Double> tsList, int paaSize) {
        double[] ts = new double[tsList.size()];
        for (int i = 0; i < tsList.size(); i++) {
            ts[i] = tsList.get(i);
        }

        // fix the length
        int len = ts.length;
        if (len < paaSize) {
            System.out.println("PAA size can't be greater than the timeseries size.");
            return null;
        }
        // check for the trivial case
        if (len == paaSize) {
            return Arrays.copyOf(ts, ts.length);
        } else {
            double[] paa = new double[paaSize];
            double pointsPerSegment = (double) len / (double) paaSize;
            double[] breaks = new double[paaSize + 1];
            for (int i = 0; i < paaSize + 1; i++) {
                breaks[i] = i * pointsPerSegment;
            }

            for (int i = 0; i < paaSize; i++) {
                double segStart = breaks[i];
                double segEnd = breaks[i + 1];

                double fractionStart = Math.ceil(segStart) - segStart;
                double fractionEnd = segEnd - Math.floor(segEnd);

                int fullStart = Double.valueOf(Math.floor(segStart)).intValue();
                int fullEnd = Double.valueOf(Math.ceil(segEnd)).intValue();

                double[] segment = Arrays.copyOfRange(ts, fullStart, fullEnd);

                if (fractionStart > 0) {
                    segment[0] = segment[0] * fractionStart;
                }

                if (fractionEnd > 0) {
                    segment[segment.length - 1] = segment[segment.length - 1] * fractionEnd;
                }

                double elementsSum = 0.0;
                for (double e : segment) {
                    elementsSum = elementsSum + e;
                }

                paa[i] = elementsSum / pointsPerSegment;

            }
            return paa;
        }
    }
}
