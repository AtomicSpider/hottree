package SAX;

import SAX.Util.CutType;
import SAX.Util.MethodType;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by IN0120: Sanat Dutta on 22-05-2017.
 */
public class SaxUtil {

    private static double[] hotSaxCutsArray;
    private static double[] satCutsArray;
    private static String method;

    public static double[] generateCuts(ArrayList<Double> ts, double[] paaLimits, CutType ct, int alphabetSize, MethodType methodType, double[] cus) {
        if (methodType == MethodType.HOTSAX) method = "HOTSAX ";
        else if (methodType == MethodType.SAT) method = "SAT ";

        if (alphabetSize > 20 || alphabetSize < 2) {
            System.out.println("Error: Max 20 chars, Min 2 chars");
            return null;
        }

        double[] tempArray = null;
        if (ct == CutType.GAUSSIAN_CUT) {
            switch (alphabetSize) {
                case 2:
                    tempArray = case2.clone();
                    break;
                case 3:
                    tempArray = case3.clone();
                    break;
                case 4:
                    tempArray = case4.clone();
                    break;
                case 5:
                    tempArray = case5.clone();
                    break;
                case 6:
                    tempArray = case6.clone();
                    break;
                case 7:
                    tempArray = case7.clone();
                    break;
                case 8:
                    tempArray = case8.clone();
                    break;
                case 9:
                    tempArray = case9.clone();
                    break;
                case 10:
                    tempArray = case10.clone();
                    break;
                case 11:
                    tempArray = case11.clone();
                    break;
                case 12:
                    tempArray = case12.clone();
                    break;
                case 13:
                    tempArray = case13.clone();
                    break;
                case 14:
                    tempArray = case14.clone();
                    break;
                case 15:
                    tempArray = case15.clone();
                    break;
                case 16:
                    tempArray = case16.clone();
                    break;
                case 17:
                    tempArray = case17.clone();
                    break;
                case 18:
                    tempArray = case18.clone();
                    break;
                case 19:
                    tempArray = case19.clone();
                    break;
                case 20:
                    tempArray = case20.clone();
                    break;
            }
        } else if (ct == CutType.LINEAR_CUT) {
            double minSeries = min(ts);
            double maxSeries = max(ts);
           /* if (cus != null) {
                minSeries = cus[0];
                maxSeries = cus[1];
            } else {
                minSeries = paaLimits[0];
                maxSeries = paaLimits[1];
            }*/
            double step = (maxSeries - minSeries) / (alphabetSize - 2);

            System.out.println(method + "Linear Cut Min: " + minSeries);
            System.out.println(method + "Linear Cut Max: " + maxSeries);
            System.out.println(method + "Linear Cut Step: " + step);

            tempArray = new double[alphabetSize - 1];
            for (int i = 0; i < tempArray.length; i++) {
                tempArray[i] = minSeries + (i * step);
            }
        } else if (ct == CutType.CUSTOM_CUT) {
            tempArray = cus.clone();
        }

        if (methodType == MethodType.HOTSAX)
            hotSaxCutsArray = tempArray.clone();
        else if (methodType == MethodType.SAT)
            satCutsArray = tempArray.clone();

        return tempArray;
    }

    public static double min(ArrayList<Double> series) {
        double min = Double.MAX_VALUE;
        for (Double sery : series) {
            if (min > sery) {
                min = sery;
            }
        }
        return min;
    }

    public static double min(double[] series) {
        double min = Double.MAX_VALUE;
        for (Double sery : series) {
            if (min > sery) {
                min = sery;
            }
        }
        return min;
    }

    public static double max(ArrayList<Double> series) {
        double max = Double.MIN_VALUE;
        for (Double sery : series) {
            if (max < sery) {
                max = sery;
            }
        }
        return max;
    }

    public static double max(double[] series) {
        double max = Double.MIN_VALUE;
        for (Double sery : series) {
            if (max < sery) {
                max = sery;
            }
        }
        return max;
    }

    public static double mean(ArrayList<Double> series) {
        double res = 0D;
        int count = 0;
        for (double tp : series) {
            res += tp;
            count += 1;

        }
        if (count > 0) {
            return res / ((Integer) count).doubleValue();
        }
        return Double.NaN;
    }

    public static double mean(double[] series) {
        double res = 0D;
        int count = 0;
        for (double tp : series) {
            res += tp;
            count += 1;

        }
        if (count > 0) {
            return res / ((Integer) count).doubleValue();
        }
        return Double.NaN;
    }

    public static double median(ArrayList<Double> seriesList) {
        double[] series = new double[seriesList.size()];
        for (int i = 0; i < seriesList.size(); i++) {
            series[i] = seriesList.get(i);
        }
        double[] clonedSeries = series.clone();
        Arrays.sort(clonedSeries);

        double median;
        if (clonedSeries.length % 2 == 0) {
            median = (clonedSeries[clonedSeries.length / 2]
                    + (double) clonedSeries[clonedSeries.length / 2 - 1]) / 2;
        } else {
            median = clonedSeries[clonedSeries.length / 2];
        }
        return median;
    }

    public static double var(ArrayList<Double> series) {
        double res = 0D;
        double mean = mean(series);
        int count = 0;
        for (double tp : series) {
            res += (tp - mean) * (tp - mean);
            count += 1;
        }
        if (count > 0) {
            return res / ((Integer) (count - 1)).doubleValue();
        }
        return Double.NaN;
    }

    public static double stDev(ArrayList<Double> series) {
        double num0 = 0D;
        double sum = 0D;
        int count = 0;
        for (double tp : series) {
            num0 = num0 + tp * tp;
            sum = sum + tp;
            count += 1;
        }
        double len = ((Integer) count).doubleValue();
        return Math.sqrt((len * num0 - sum * sum) / (len * (len - 1)));
    }

    public static ArrayList<Double> znorm(ArrayList<Double> series, double normalizationThreshold) {
        ArrayList<Double> res = new ArrayList<>();
        double mean = mean(series);
        double sd = stDev(series);
        if (sd < normalizationThreshold) {
            for (Double sery : series) {
                res.add(sery - mean);
            }
        } else {
            for (Double sery : series) {
                res.add((sery - mean) / sd);
            }
        }
        return res;
    }

    private static final double[] case2 = {0};
    private static final double[] case3 = {-0.4307273, 0.4307273};
    private static final double[] case4 = {-0.6744898, 0, 0.6744898};
    private static final double[] case5 = {-0.841621233572914, -0.2533471031358, 0.2533471031358,
            0.841621233572914};
    private static final double[] case6 = {-0.967421566101701, -0.430727299295457, 0,
            0.430727299295457, 0.967421566101701};
    private static final double[] case7 = {-1.06757052387814, -0.565948821932863, -0.180012369792705,
            0.180012369792705, 0.565948821932863, 1.06757052387814};
    private static final double[] case8 = {-1.15034938037601, -0.674489750196082, -0.318639363964375,
            0, 0.318639363964375, 0.674489750196082, 1.15034938037601};
    private static final double[] case9 = {-1.22064034884735, -0.764709673786387, -0.430727299295457,
            -0.139710298881862, 0.139710298881862, 0.430727299295457, 0.764709673786387,
            1.22064034884735};
    private static final double[] case10 = {-1.2815515655446, -0.841621233572914, -0.524400512708041,
            -0.2533471031358, 0, 0.2533471031358, 0.524400512708041, 0.841621233572914, 1.2815515655446};
    private static final double[] case11 = {-1.33517773611894, -0.908457868537385,
            -0.604585346583237, -0.348755695517045, -0.114185294321428, 0.114185294321428,
            0.348755695517045, 0.604585346583237, 0.908457868537385, 1.33517773611894};
    private static final double[] case12 = {-1.38299412710064, -0.967421566101701,
            -0.674489750196082, -0.430727299295457, -0.210428394247925, 0, 0.210428394247925,
            0.430727299295457, 0.674489750196082, 0.967421566101701, 1.38299412710064};
    private static final double[] case13 = {-1.42607687227285, -1.0200762327862, -0.736315917376129,
            -0.502402223373355, -0.293381232121193, -0.0965586152896391, 0.0965586152896394,
            0.293381232121194, 0.502402223373355, 0.73631591737613, 1.0200762327862, 1.42607687227285};
    private static final double[] case14 = {-1.46523379268552, -1.06757052387814, -0.791638607743375,
            -0.565948821932863, -0.36610635680057, -0.180012369792705, 0, 0.180012369792705,
            0.36610635680057, 0.565948821932863, 0.791638607743375, 1.06757052387814, 1.46523379268552};
    private static final double[] case15 = {-1.50108594604402, -1.11077161663679, -0.841621233572914,
            -0.622925723210088, -0.430727299295457, -0.2533471031358, -0.0836517339071291,
            0.0836517339071291, 0.2533471031358, 0.430727299295457, 0.622925723210088, 0.841621233572914,
            1.11077161663679, 1.50108594604402};
    private static final double[] case16 = {-1.53412054435255, -1.15034938037601, -0.887146559018876,
            -0.674489750196082, -0.488776411114669, -0.318639363964375, -0.157310684610171, 0,
            0.157310684610171, 0.318639363964375, 0.488776411114669, 0.674489750196082, 0.887146559018876,
            1.15034938037601, 1.53412054435255};
    private static final double[] case17 = {-1.5647264713618, -1.18683143275582, -0.928899491647271,
            -0.721522283982343, -0.541395085129088, -0.377391943828554, -0.223007830940367,
            -0.0737912738082727, 0.0737912738082727, 0.223007830940367, 0.377391943828554,
            0.541395085129088, 0.721522283982343, 0.928899491647271, 1.18683143275582, 1.5647264713618};
    private static final double[] case18 = {-1.59321881802305, -1.22064034884735, -0.967421566101701,
            -0.764709673786387, -0.589455797849779, -0.430727299295457, -0.282216147062508,
            -0.139710298881862, 0, 0.139710298881862, 0.282216147062508, 0.430727299295457,
            0.589455797849779, 0.764709673786387, 0.967421566101701, 1.22064034884735, 1.59321881802305};
    private static final double[] case19 = {-1.61985625863827, -1.25211952026522, -1.00314796766253,
            -0.8045963803603, -0.633640000779701, -0.47950565333095, -0.336038140371823,
            -0.199201324789267, -0.0660118123758407, 0.0660118123758406, 0.199201324789267,
            0.336038140371823, 0.47950565333095, 0.633640000779701, 0.8045963803603, 1.00314796766253,
            1.25211952026522, 1.61985625863827};
    private static final double[] case20 = {-1.64485362695147, -1.2815515655446, -1.03643338949379,
            -0.841621233572914, -0.674489750196082, -0.524400512708041, -0.385320466407568,
            -0.2533471031358, -0.125661346855074, 0, 0.125661346855074, 0.2533471031358,
            0.385320466407568, 0.524400512708041, 0.674489750196082, 0.841621233572914, 1.03643338949379,
            1.2815515655446, 1.64485362695147};
}
