package SAX;

/**
 * Created by IN0120: Sanat Dutta on 22-05-2017.
 */
public class Util {

    public enum CutType {
        GAUSSIAN_CUT,
        LINEAR_CUT,
        CUSTOM_CUT
    }

    public enum MethodType {
        HOTSAX,
        SAT
    }
}
